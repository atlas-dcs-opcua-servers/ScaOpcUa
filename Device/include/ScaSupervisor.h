/*
 * ScaSupervisor.h
 *
 *  Created on: 8 Aug 2018
 *      Author: pnikiel
 */

#ifndef DEVICE_SRC_SCASUPERVISOR_H_
#define DEVICE_SRC_SCASUPERVISOR_H_

#include <GhostPointer.h>
#include <list>
#include <mutex>
#include <thread>
#include <functional>

#include <Sca/Sca.h>


typedef std::function<void(bool pongs, double rttUs)> ScaSupervisorNotification;
typedef std::function<void(Sca::Sca& sca)> ScaInitializeFunction;

enum RecoveryAction
{
    DO_NOTHING,
    CONFIGURE,
    RESET_AND_CONFIGURE
};

enum ResetFromAddressSpace
{
    DENIED,
    UNCONDITIONAL,
    ONLY_IF_KAPUTT
};

class ScaSupervisor
//! This is a singleton.
{
public:
    virtual ~ScaSupervisor();

    //! This should be called when server is finished loading configuration and normal operation could start,
    //! The supervisor doesn't do anything before this is called.
    void onServerInitialize();

    static ScaSupervisor* getInstance() { return s_instance; }
    static ScaSupervisor* instance();
    static void kill();

    void stopBackgroundTask();

    struct RequireRequest
    {
        std::string               address;
        ScaInitializeFunction     initializeFunction;
        ScaSupervisorNotification notification;
        bool                      checkId;
        unsigned int              requestedId;
        RecoveryAction            recoveryActionScaStayedPowered;
        RecoveryAction            recoveryActionScaWasRepowered;
    };

    GhostPointer<Sca::Sca> requireSca (const RequireRequest& request);
    void backgroundTask();
    void printSummary();
    unsigned int getNumberOffline();

    bool requireReset( const std::string& address, bool onlyIfKaputt);

    static RecoveryAction parseRecoveryAction( const std::string& action );
    static std::string recoveryActionToString( const RecoveryAction& action );
    static ResetFromAddressSpace parseResetFromAddressSpace ( const std::string& resetFromAddressSpace );

private:
    ScaSupervisor();



    struct InitializedScaHolder
    {
        InitializedScaHolder(
                const RequireRequest&            aRequest):
            request(aRequest),
            sca(request.address),
            lastPongState(true) // if we managed to construct the SCA object then we assume the communication was OK
        {}
        RequireRequest            request;
        Sca::Sca                  sca;
        GhostPointer<Sca::Sca>    reference;
        bool                      lastPongState;
    };

    struct DeferredScaHolder
    {
        RequireRequest          request;
        GhostPointer<Sca::Sca>  reference;
    };

    bool attemptInitialize(
            const RequireRequest&   request,
            GhostPointer<Sca::Sca>& target,
            bool                    runInitializerDelegate=true
            );
    void processDeferred();
    void processInitialized();
    void processExternalRequests();

    void act(
            const RecoveryAction& action,
            InitializedScaHolder& holder);

    static std::mutex s_instanceControlLock;
    static ScaSupervisor* s_instance;
    std::list<InitializedScaHolder> m_scas;
    std::list<DeferredScaHolder>    m_deferredScas;
    std::thread m_backgroundThread;
    bool m_continueBackgroundTask;
    std::mutex m_accessLock;

    unsigned int m_numberOffline;


    struct ExternalResetRequest
    {
        ExternalResetRequest (const std::string& anAddress, bool onlyIfKaputt):
            address(anAddress),
            finished(false),
            resetOnlyIfKaputt(onlyIfKaputt),
            finishedSuccessfully(false) {}
        std::string  address;
        bool         finished;
        bool         resetOnlyIfKaputt;
        bool         finishedSuccessfully;
    };

    std::list<ExternalResetRequest> m_externalResetRequests;
    std::mutex m_externalResetRequestsLock;

    static Log::LogComponentHandle s_logComponent;

};

#endif /* DEVICE_SRC_SCASUPERVISOR_H_ */
