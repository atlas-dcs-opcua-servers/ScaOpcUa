echo "make_distro: trying to create a package for commit id [$1] and for platform [$2]"

commit_id=$1
platform=$2
package_dir="OpcUaScaServer-$commit_id-$platform"
package_file="$package_dir.tar"

# Remove old package and directory
rm -Rf "$package_dir" "$package_file"

# Create necessary directories
mkdir -p "$package_dir/bin"
mkdir -p "$package_dir/lib"
mkdir -p "$package_dir/Configuration"
mkdir -p "$package_dir/Documentation"
mkdir -p "$package_dir/bin/ScaSoftware"

cp -v build/bin/OpcUaScaServer \
    bin/ServerConfig.xml \
    bin/config-sim.xml \
    felixClientProperties_example.sh \
    $package_dir/bin || exit 1

cp -v build/Configuration/Configuration.xsd  \
    $package_dir/Configuration || exit 1

cp -vf Documentation/ConfigDocumentation.html \
    Documentation/AddressSpaceDoc.html \
    $package_dir/Documentation || echo -e "\033[33;1mWarning: The documentation files were not copied successfully. Continuing...\033[0m"

cp -vL ${FELIX_ROOT}/lib/libfelix-client-thread.so \
    ${FELIX_ROOT}/lib/libnetio.so \
    ${FELIX_ROOT}/lib/libhdlc_coder.so \
    ${FELIX_ROOT}/lib/libtbb.so.2 \
    ${FELIX_ROOT}/lib/libfabric.so.1 \
    ${FELIX_ROOT}/lib/libatomic.so.1 \
    ${FELIX_ROOT}/lib/libfelix-client-lib.so \
    ${FELIX_ROOT}/lib/libnetio-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-server-lib.so \
    ${FELIX_ROOT}/lib/libjwrite.so \
    ${FELIX_ROOT}/lib/libsimdjson.so \
    $package_dir/lib || exit 1

cp -vL $PROTOBUF_DIRECTORIES/libprotobuf.so.* \
    $BOOST_ROOT/lib/libboost_atomic*.so.* \
    $BOOST_ROOT/lib/libboost_date_time*.so.* \
    $BOOST_ROOT/lib/libboost_chrono*.so.* \
    $BOOST_ROOT/lib/libboost_filesystem*.so.* \
    $BOOST_ROOT/lib/libboost_program_options*.so.* \
    $BOOST_ROOT/lib/libboost_regex*.so.* \
    $BOOST_ROOT/lib/libboost_system*.so.* \
    $BOOST_ROOT/lib/libboost_thread*.so.* \
    "`dirname $CXX`/../lib64/libstdc++.so.6" \
    "`dirname $CXX`/../lib64/libgcc_s.so.1" \
    $package_dir/lib/ || exit 1

cp -vL /usr/lib64/libxerces-c.so $package_dir/lib/ || exit 1

cp -vL ScaSoftware/build/Demonstrators/StandaloneI2cSimulation/standalone_i2c \
    ScaSoftware/build/Demonstrators/GbtxConfiguration/gbtx_configuration \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fscajtag \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fscajfile \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fxvcserver \
    ScaSoftware/build/Demonstrators/Timing/timing \
    ScaSoftware/Demonstrators/README.md $package_dir/bin/ScaSoftware

# Create the tarball and clean up
tar cf "$package_file" "$package_dir"
gzip -9 "$package_file"
rm -rf "$package_dir"